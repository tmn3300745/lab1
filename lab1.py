import pandas as pd
import time
import os
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, \
                            precision_score, \
                            recall_score, \
                            f1_score, \
                            log_loss, \
                            precision_recall_curve, \
                            roc_curve


# read data and split them to train and test parts
def prepare_data(path, test_size, random_state=322):
    data = pd.read_csv(path)
    
    x = data.drop(columns=['Activity'])
    y = data['Activity']
    
    return train_test_split(x, y, test_size=test_size, random_state=random_state)
    

def create_train_classificators(x_train, y_train, dtc_deep_max_depth, rfc_simple_n_estimators, rfc_deep_n_estimators, rfc_deep_max_depth):
    classificators = {
        'dtc_simple': DecisionTreeClassifier(),
        'dtc_deep': DecisionTreeClassifier(max_depth=dtc_deep_max_depth),
        'rfc_simple': RandomForestClassifier(n_estimators=rfc_simple_n_estimators),
        'rfc_deep': RandomForestClassifier(n_estimators=rfc_deep_n_estimators, max_depth=rfc_deep_max_depth)
    }
    
    for classificator in classificators.values():
        classificator.fit(x_train, y_train)
        
    return classificators


def measure_of_classificator(classificator, x_test, y_test, t=None):
    #custom Threshold
    if t:
        y_predicted = adjusted_classificator(classificator, x_test, t)
    else:
        y_predicted = classificator.predict(x_test)
    
    return {
        'Accuracy': accuracy_score(y_test, y_predicted),
        'Precision': precision_score(y_test, y_predicted),
        'Recall': recall_score(y_test, y_predicted),
        'F1_Score': f1_score(y_test, y_predicted),
        'Log_Loss': log_loss(y_test, y_predicted)
    }


def pr_curves(classificators, x_test, y_test, dest):
    properties = [precision_recall_curve(y_test, classificator.predict_proba(x_test)[:,1]) for classificator in classificators.values()]
    names = list(classificators.keys())
    plt.figure(figsize=(12,12))
    for i, property in enumerate(properties):
        plt.plot(property[2], property[1][:-1], label=f'Recall ({names[i]})')
        plt.plot(property[2], property[0][:-1], label=f'Precision ({names[i]})')
    
    plt.xlabel('Threshold')
    plt.ylabel('Recall / Precision')
    plt.title('Precision-Recall Curves')
    plt.legend()
    plt.tight_layout()
    plt.savefig(f'{dest}/pr_curve.png')
    
    
def roc_curves(classificators, x_test, y_test, dest):
    properties = [roc_curve(y_test, classificator.predict_proba(x_test)[:,1]) for classificator in classificators.values()]
    names = list(classificators.keys())
    plt.figure(figsize=(12,12))
    for i, property in enumerate(properties):
        plt.plot(property[0], property[1], label=f'{names[i]}')
    
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title("ROC-curves")
    plt.legend()
    plt.tight_layout()
    plt.savefig(f'{dest}/roc_curve.png')
    
    
# custom Threshold for classificators
def adjusted_classificator(classificator, x_test, t):
    return [1 if y_pred>=t else 0 for y_pred in classificator.predict_proba(x_test)[:,1]]


if __name__ == "__main__":
    x_train, x_test, y_train, y_test = prepare_data('./data/bioresponse.csv', 0.3)
    
    classificators = create_train_classificators(x_train, y_train, 10, 100, 100, 10)
    
    timestr = time.strftime("%Y%m%d-%H%M%S")
    os.mkdir(f'./output/{timestr}')
    with open(f'./output/{timestr}/measures', 'w') as f:
        for name, classificator in classificators.items():
            f.write(f'Results for {name}: {measure_of_classificator(classificator, x_test, y_test)}\n')
        # Results for custom Threshold
        f.write(f'Results for adjusted classificator(rfc_deep): {measure_of_classificator(classificators["rfc_deep"], x_test, y_test, t=0.11)}\n')
            
    pr_curves(classificators, x_test, y_test, f'./output/{timestr}')
    roc_curves(classificators, x_test, y_test, f'./output/{timestr}')
